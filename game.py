from random import randint

# user input name ("Hi! What is your name?")
# store name in variable
user_name = input("Hi! What is your name? ")

# Guess up to 5 times
for attempt in range(1, 6):
    # Generate random day
    rand_day = randint(1, 31)
    # Generate random int for month with specific month name
    months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ]
    rand_num = randint(0, 11)
    rand_month = months[rand_num]
    # Generate random int for year between 1924 and 2004
    rand_year = randint(1924, 2004)
    # Print guess statement for human response
    print(f"Guess {attempt} : {user_name} were you born in {rand_month} {rand_day}, {rand_year} ?")
    # Get user input for yes or no after computer guess
    user_ans = input("yes or no? ")
    # Print statements based on conditions
    if user_ans == "yes":
        print("I knew it!")
        break
    elif user_ans == "no" and attempt < 5:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
